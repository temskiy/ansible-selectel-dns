Модуль для работы с DNS-записями Selectel
---------------------------------------

[Документация на API](https://developers.selectel.ru/docs/cloud-services/dns_api/)

Зависимости: [api-selectel](https://pypi.org/project/api-selectel/)


Возможности
-----------

Добавляет или удаляет запись типа SOA, NS, A, AAAA, CNAME, SRV, MX, TXT, SPF в указанной DNS-зоне. 

Установка
---------

Либо скопировать **library/selectel_dns_records.py** в каталог action-плагинов, либо скопировать **library** в каталог роли или плэйбука.


Параметры
---------

**token**  -  [аутентификационный токен Selectel](https://developers.selectel.ru/docs/control-panel/authorization/) 

**content**  -  Содержимое записи (нет у SRV)

**email**  -  RNAME decoded e-mail администратора домена (только у SOA)

**name**  -  Имя записи

**port**  -  Порт сервиса (только у SRV)

**priority**  -  Приоритет записи (только у MX и SRV)

**target**  -  Имя хоста сервиса (только у SRV)

**ttl**  -  Время жизни

**type**  -  Тип записи (SOA, NS, A/AAAA, CNAME, SRV, MX, TXT, SPF)

**weight**  -  Относительный вес для записей с одинаковым приоритетом (только у SRV)


Примеры использования
---------------------

```yml
- name: Add test record
  selectel_dns_records:
    state: present
    token: "***token***"
    content: "127.0.0.1"
    name: "test67.example.com"
    domain: "example.com"
    type: "A"

- name: Remove test record
  selectel_dns_records:
    state: absent
    token: "***token***"
    content: "127.0.0.1"
    name: "test67.example.com"
    domain: "example.com"
    type: "A"
```
