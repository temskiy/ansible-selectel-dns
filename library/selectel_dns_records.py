#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: (c) 2023 Artem Gnatyuk <temskiy@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r'''
---
module: selectel_dns_records
author:
- Artem Gnatyuk (temskiy)
requirements:
   - python >= 3.0
   - api-selectel >= 0.1.0

short_description: Manage DNS records in Selectel
description:
   - "Manages dns records via API, see the docs: U(https://developers.selectel.ru/docs/cloud-services/dns_api/)."
options:
  token:
    description:
    - Required for authentification.
    - Must be created as described in documetation.
    type: str
    required: true
    aliases: [ auth_token ]
  content:
    description:
    - Record content.
    - Not required for SRV record.
    type: str
    required: false
    aliases: [ value ]
  email:
    description:
    - RNAME decoded e-mail domain administrator.
    - Required only for SOA record.
    type: str
    required: false
  name:
    description:
    - FQDN of record to add.
    - Default is C(@) (e.g. the zone name).
    type: str
    default: '@'
    aliases: [ record ]
  port:
    description:
    - Service port
    - Required only for SRV record.
    type: int
  priority:
    description:1
    - Record priority
    - Required only for SRV and MX record.
    type: int
  target:
    description:
    - Service hostname
    - Required only for SRV record.
    type: str
  ttl:
    description:
    - The TTL to give the new record.
    type: int
    default: 3600
  type:
    description:
      - The type of DNS record to create. Required if C(state=present).
    type: str
    choices: [ SOA, NS, A, AAAA, CNAME, SRV, MX, TXT, SPF ]
    required: true
  weight:
    description:
    - Ralative weight for records with equal priority
    - Required only for SRV record.
    type: str
  zone:
    description:
    - FQDN of domain.
    type: str
    aliases: [ domain ]
  state:
    description:
    - Whether the record(s) should exist or not.
    type: str
    choices: [ absent, present ]
    default: present
'''

EXAMPLES = r'''
- name: Add test record
  selectel_dns_records:
    state: present
    token: "***token***"
    content: "127.0.0.1"
    name: "test67.example.com"
    domain: "example.com"
    type: "A"

- name: Remove test record
  selectel_dns_records:
    state: absent
    token: "***token***"
    content: "127.0.0.1"
    name: "test67.example.com"
    domain: "example.com"
    type: "A"
'''

RETURN = ''

import api_selectel
from ansible.module_utils.basic import AnsibleModule, env_fallback


def main():
    module = AnsibleModule(
        argument_spec=dict(
            token=dict(type="str", required=True, no_log=True, aliases=['auth_token'], fallback=(env_fallback, ["SELECTEL_TOKEN"]),),
            content=dict(type='str', aliases=['value']),
            email=dict(type='str'),
            name=dict(type='str', default='@', aliases=['record']),
            port=dict(type='int'),
            priority=dict(type='int'),
            target=dict(type='str'),
            ttl=dict(type='int', default=3600),
            type=dict(type='str', required=True, choices=["SOA", "NS", "A", "AAAA", "CNAME", "SRV", "MX", "TXT", "SPF"]),
            weight=dict(type='int'),
            zone=dict(type='str', aliases=['domain']),
            state=dict(type='str', default='present', choices=['absent', 'present']),
        ),
        supports_check_mode=False,
    )
    api=api_selectel.Api_Selectel(token=module.params['token'])
    domain_id=api.domain_id(module.params['zone'])
    record_id=api.record_id(domain=domain_id, record_name=module.params['name'], record_type=module.params['type'], content=module.params['content'])
    changed=False
    if module.params['state'] == 'present':
      if record_id == 0:
        r, s, h = api.records_create(domain_id=domain_id, content=module.params['content'], email=module.params['email'], name=module.params['name'], port=module.params['port'], priority=module.params['priority'], target=module.params['target'], ttl=module.params['ttl'], type=module.params['type'])
        if s >= 400:
          module.fail_json(msg=str(s) + str(h))
        changed=True
    else:
      if record_id != 0:
        r, s, h = api.records_delete(domain_id=domain_id, record_id=record_id)
        if s >= 400:
          module.fail_json(msg=str(s) + str(h))
        changed=True
    module.exit_json(changed=changed)

if __name__ == '__main__':
    main()
